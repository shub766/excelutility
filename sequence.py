import cx_Oracle
import pandas as pd
from datetime import datetime
import conf
try:
    db_config = conf.dbconfig()
    con = cx_Oracle.connect(db_config['user'] + '/' + db_config['password'] + '@' + db_config['connectString'])
    print(con)
    cursor = con.cursor()
    df = pd.read_excel('./files/IN/MUMS86155G.TV919_Compliance_Check_206AB_206CCA_22-06-2021_1.xlsx', skiprows=4)
    # print(df)
    data = df.values.tolist()
    # print(data[0])
    res = data[:len(data)-2]
    print(res)
    # sql_query = ("INSERT INTO mfx_itr_upl_data(col_1, pan, name, pan_allotment_date, pan_aadhaar_link_status,specified_person_206ab_206cca)" "VALUES(:1,:2,:3,:4,:5,:6)")
    q = "INSERT INTO mfx_itr_upl_data (batch_no,record_no,col_1, pan, name, pan_allotment_date, pan_aadhaar_link_status,specified_person_206ab_206cca) VALUES(1,1,:1, :2, :3, :4, :5, :6)"
    cursor.executemany(q,res)
    con.commit()


except cx_Oracle.DatabaseError as e:
    print("There is a problem with Oracle", e)

    # by writing finally if any error occurs
    # then also we can close the all database operation
finally:
    print("finally")
    # if cursor:
    #     cursor.close()
    # if con:
    #     con.close()