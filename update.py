import cx_Oracle
from datetime import datetime
import random
import string
import conf

def update(upload_status,upload_error,log_id):
    try:
        db_config = conf.dbconfig()
        con = cx_Oracle.connect(db_config['user'] + '/' + db_config['password'] + '@' + db_config['connectString'])
        cursor = con.cursor()
        sql = 'update  mfx_itr_upl_log set upload_status=:1,upload_error= :2 where log_id = :3'
        cursor.execute(sql,[upload_status,str(upload_error),log_id])
        print('Record inserted successfully')
        con.commit()

    except cx_Oracle.DatabaseError as e:
        print("There is a problem with Oracle", e)

    finally:
        if cursor:
            cursor.close()
        if con:
            con.close()



def insert(f):
    print("IN insert")
    try:
        db_config = conf.dbconfig()
        con = cx_Oracle.connect(db_config['user'] + '/' + db_config['password'] + '@' + db_config['connectString'])
        cursor = con.cursor()
        print(con)
        date = datetime.today()
        filename = f
        ran = ''.join(random.choices(string.ascii_uppercase + string.digits, k = 10))  
        q = "INSERT INTO mfx_itr_upl_log (log_id,file_name,uploaded_by,upload_date,upload_status,upload_error,batch_no,total_record) VALUES(:log_id, :file_name, :uploaded_by, :upload_date, :upload_status, :upload_error,:batch_no,:total_record)"
        cursor.execute(q, log_id=ran, file_name=f, uploaded_by="Admin", upload_date=datetime.today(), upload_status="null", upload_error="null",batch_no="null",total_record="10002")

        con.commit()
        print('Record inserted successfully')

    except cx_Oracle.DatabaseError as e:
        print("There is a problem with Oracle", e)

    finally:
        if cursor:
            cursor.close()
        if con:
            con.close()


def udate_invalid(f):
    try:
        db_config = conf.dbconfig()
        con = cx_Oracle.connect(db_config['user'] + '/' + db_config['password'] + '@' + db_config['connectString'])
        cursor = con.cursor()
        sql = 'update  mfx_itr_upl_log set upload_status=:1,upload_error= :2 where file_name = :3'
        cursor.execute(sql,['E','Invalid file',f])
        print('Record inserted successfully')
        con.commit()

    except cx_Oracle.DatabaseError as e:
        print("There is a problem with Oracle", e)

    finally:
        if cursor:
            cursor.close()
        if con:
            con.close()



def udate_batchno(batch_no,ran):
    try:
        db_config = conf.dbconfig()
        con = cx_Oracle.connect(db_config['user'] + '/' + db_config['password'] + '@' + db_config['connectString'])
        cursor = con.cursor()
        batch_no = batch_no
        sql = 'update  mfx_itr_upl_log set batch_no=:1 where log_id = :2'
        cursor.execute(sql,[batch_no,ran])
        print('Record inserted successfully')
        con.commit()
    except cx_Oracle.DatabaseError as e:
        print("There is a problem with Oracle", e)

    finally:
        if cursor:
            cursor.close()
        if con:
            con.close()

def getbatchno(ran):
    db_config = conf.dbconfig()
    con = cx_Oracle.connect(db_config['user'] + '/' + db_config['password'] + '@' + db_config['connectString'])
    print(con)
    cursor = con.cursor()
    sql = 'SELECT * from mfx_itr_upl_log where log_id = :1 '
    cursor.execute(sql,[ran])
    row = cursor.fetchone()
    return row[len(row)-2]
